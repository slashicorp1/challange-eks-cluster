data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_id
}

data "aws_availability_zones" "available" {
}

locals {
  cluster_name = "eks-private-${random_string.suffix.result}"
}

locals {
  environment = "${random_string.suffix.result}"
}

resource "random_string" "suffix" {
  length  = 8
  special = false
}

module "eks" {
  source          = "terraform-aws-modules/eks/aws"
  cluster_name    = local.cluster_name
  cluster_version = "1.19"
  subnets         = var.private_subnets
  worker_create_cluster_primary_security_group_rules = true
  cluster_endpoint_private_access = true
  cluster_endpoint_public_access = true

  tags = {
    Environment = "Homolog"
  }

  vpc_id = var.vpc_id

  node_groups_defaults = {
    ami_type  = "AL2_x86_64"
    disk_size = 50
  }

  node_groups = {
    mdl-nodes = {
      desired_capacity = 3
      max_capacity     = 5
      min_capacity     = 3

      instance_types = ["t3.medium"]
      capacity_type  = "SPOT"
      k8s_labels = {
        Environment = "Homolog"
      }
    }
  }
}

module "security-group-ipsec-vpn" {
  source                = "terraform-aws-modules/security-group/aws"
  name                  = "ipsec-vpn-sg-${local.environment}"
  description           = "Security group for allow IPSec VPN"
  vpc_id                = var.vpc_id
  ingress_cidr_blocks   = ["0.0.0.0/0"]
  ingress_rules         = ["all-all"]
  egress_rules          = ["all-all"]
}
module "ec2-instance-ipsec-vpn" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  name                   = "gateway-ipsec-${local.environment}"
  instance_count         = 1
  ami                    = "ami-05044553dd98dc31c"
  instance_type          = "t2.small"
  key_name               = "infrastructure-mundiale"
  monitoring             = true
  vpc_security_group_ids =  [module.security-group-ipsec-vpn.security_group_id]
  subnet_id              = tolist(var.public_subnets)[0]
  tags = {
    Owner       = "Mundiale"
    Vendor      = "Estabilis"
    Environment = "Homolog"
    Terraform   = "true"
  }
}
module "security-group-bastion" {
  source                = "terraform-aws-modules/security-group/aws"
  name                  = "bastion-sg-${local.environment}"
  description           = "Security group for allow SSH access in BASTION"
  vpc_id                = var.vpc_id
  ingress_cidr_blocks   = ["0.0.0.0/0"]
  ingress_rules         = ["all-all"]
  egress_rules          = ["all-all"]
}
module "ec2-instance-bastion" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  name                   = "bastion-${local.environment}"
  instance_count         = 1
  ami                    = "ami-042e8287309f5df03"
  instance_type          = "t3.micro"
  key_name               = "infrastructure-mundiale"
  monitoring             = true
  vpc_security_group_ids = [module.security-group-bastion.security_group_id]
  subnet_id              = tolist(var.public_subnets)[0]
  tags = {
    Owner       = "Mundiale"
    Vendor      = "Estabilis"
    Environment = "Homolog"
    Terraform   = "true"
  }
}
