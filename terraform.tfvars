aws_access_key = "${AWS_ACCESS_KEY}"
aws_secret_key = "${AWS_SECRET_KEY}"
aws_region = "${AWS_REGION}"
private_subnets = [
  "${SUBNET_PRIVATE_A}",
  "${SUBNET_PRIVATE_B}",
  "${SUBNET_PRIVATE_C}",
]
public_subnets = [
  "${SUBNET_PUBLIC_D}",
  "${SUBNET_PUBLIC_E}",
  "${SUBNET_PUBLIC_F}",
]
vpc_id = "${VPC_ID}"
