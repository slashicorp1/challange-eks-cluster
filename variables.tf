variable "aws_access_key" {
  type        = string
  description = "AWS access key used to create infrastructure"
}

variable "aws_secret_key" {
  type        = string
  description = "AWS secret key used to create AWS infrastructure"
}

variable "aws_region" {
  type        = string
  description = "AWS region used for all resources"
}

variable "private_subnets" {
  type        = list(string)
  description = "Private subnets IP's"
}

variable "public_subnets" {
  type        = list(string)
  description = "Public subnets IP's"
}

variable "vpc_id" {
  type        = string
  description = "AWS VPC ID"
}
